<?php
class categorias_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_categorias($id = FALSE)
	{
        $query = $this->db->get('categorias');
        
        if ($id)
        {
	        $query = $this->db->get_where('categorias', array('id' => $id));
        }

        return $id ? $query->result() : $query->result_array();
	}

	public function set_categorias($categoria)
	{
		$existe_categoria = isset($categoria['id']) ? $this->get_categorias($categoria['id']) : false;

	    if(!$existe_categoria){
		    $data = array(
		        'nome' => $categoria['nome']
		    );

		    return $this->db->insert('categorias', $data);	    	
	    }else{
	    	$data = array(
	    	    'nome' => $categoria['nome']
	    	);
	    	$this->db->where('id',$categoria['id']);
	    	return $this->db->update('categorias', $data);
	    }
	}

	public function delete_categorias($id = FALSE)
	{
		return $this->db->delete('categorias', array('id' => $id));
	}
}