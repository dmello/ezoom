<?php
class jogos_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_jogos($id = FALSE, $order = FALSE)
	{
        
        if ($id){
	        $query = $this->db->get_where('jogos', array('id' => $id));
        }

        if($order){
        	$this->db->order_by($order);
        }else{
        	$this->db->order_by('nome ASC');
        }

        $query = $this->db->get('jogos');
        return $id ? $query->result() : $query->result_array();
	}

	public function get_categorias($id = FALSE){
        $query = $this->db->get('categorias');
        
        if ($id){
	        $query = $this->db->get_where('categorias', array('id' => $id));
        }

        return $id ? $query->row_array() : $query->result_array();
	}

	public function set_jogos($jogo)
	{
		$existe_jogo = isset($jogo['id']) ? $this->get_jogos($jogo['id']) : false;

	    if(!$existe_jogo){
		    $data = array(
		        'nome' => $jogo['nome'],
		        'ano_publicacao' => $jogo['ano_publicacao'],
		        'descricao' => $jogo['descricao'],
		        'id_categoria' => $jogo['categorias']
		    );

		    return $this->db->insert('jogos', $data);	    	
	    }else{
	    	$data = array(
	    	    'nome' => $jogo['nome'],
	    	    'ano_publicacao' => $jogo['ano_publicacao'],
	    	    'descricao' => $jogo['descricao'],
	    	    'id_categoria' => $jogo['categorias']
	    	);
	    	$this->db->where('id',$jogo['id']);
	    	return $this->db->update('jogos', $data);
	    }
	}

	public function delete_jogos($id = FALSE)
	{
		return $this->db->delete('jogos', array('id' => $id));
	}
}