<?php
class Categorias extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('categorias_model');
    }

    public function index()
    {
        $data['categorias'] = $this->categorias_model->get_categorias();
        $data['title'] = 'Lista de Categorias';

        $this->load->view('templates/header', $data);
        $this->load->view('categorias/index', $data);
        $this->load->view('templates/footer');
    }

    public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Nova categoria';

        $this->form_validation->set_rules('nome', 'nome', 'required');

        if($this->input->post()){
            if ($this->form_validation->run()){
                $post = $this->input->post();
                $this->categorias_model->set_categorias($post);
                $data['sucesso'] = "Sua categoria foi cadastrado com sucesso!";
            }
        }
        
        $this->load->view('templates/header', $data);
        $this->load->view('categorias/create', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id){
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Editar categoria';

        $this->form_validation->set_rules('nome', 'nome', 'required');
       
        $categoria = $this->categorias_model->get_categorias($id);

        if($this->input->post()){
            if ($this->form_validation->run()){
                $post = $this->input->post();
            
                if($this->categorias_model->set_categorias($post)){
                    $categoria = $this->categorias_model->get_categorias($id);
                    $data['sucesso'] = "Sua categoria foi alterada com sucesso!";
                }
            }
        }
        
        $data['categoria'] = $categoria;
        $this->load->view('templates/header', $data);
        $this->load->view('categorias/edit', $data);
        $this->load->view('templates/footer');
    }

    public function delete($id)
    {
        $data['categorias_item'] = $this->categorias_model->get_categorias($id);

        if (empty($data['categorias_item'])){
            $this->session->set_flashdata('delete', 'Esta categoria não existe.');
            redirect('categorias/listar');
        }
        else{
            if($this->categorias_item->delete_categorias($id)){
                $this->session->set_flashdata('delete', 'Sua categoria foi removida com sucesso!');
                redirect('categorias/listar');
            }
        }
    }
}