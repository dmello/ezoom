<?php
class Jogos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('jogos_model');
    }

    public function index()
    {
        $this->load->helper('form');
        if($this->input->post()){
            $data['jogos'] = $this->jogos_model->get_jogos(false,$this->input->post('ordenar_jogos'));
            $data['ordenar'] = $this->input->post('ordenar_jogos');
        }else{
            $data['jogos'] = $this->jogos_model->get_jogos(false,false);
        }
        $data['categorias'] = $this->jogos_model->get_categorias();
        $data['title'] = 'Lista de Jogos';

        $this->load->view('templates/header', $data);
        $this->load->view('jogos/index', $data);
        $this->load->view('templates/footer');
    }

    public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Novo jogo';

        $this->form_validation->set_rules('nome', 'nome', 'required');
        $this->form_validation->set_rules('ano_publicacao', 'ano_publicacao', 'required');
        $this->form_validation->set_rules('descricao', 'descricao', 'required');

        $categorias = $this->jogos_model->get_categorias();

        if($this->input->post()){
            if ($this->form_validation->run()){
                $post = $this->input->post();
                $post['ano_publicacao'] = date('Y-m-d', strtotime(str_replace('/','-', $post['ano_publicacao'])));
                $this->jogos_model->set_jogos($post);
                $data['sucesso'] = "Seu jogo foi cadastrado com sucesso!";
            }
        }
        
        $data['categorias'] = $categorias;
        $this->load->view('templates/header', $data);
        $this->load->view('jogos/create', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id){
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Editar jogo';

        $this->form_validation->set_rules('nome', 'nome', 'required');
        $this->form_validation->set_rules('ano_publicacao', 'ano_publicacao', 'required');
        $this->form_validation->set_rules('descricao', 'descricao', 'required');

        $categorias = $this->jogos_model->get_categorias();
        $jogo = $this->jogos_model->get_jogos($id);

        if($this->input->post()){
            if ($this->form_validation->run()){
                $post = $this->input->post();
                $post['ano_publicacao'] = date('Y-m-d', strtotime(str_replace('/','-', $post['ano_publicacao'])));
                
                if($this->jogos_model->set_jogos($post)){
                    $jogo = $this->jogos_model->get_jogos($id);
                    $data['sucesso'] = "Seu jogo foi alterado com sucesso!";
                }
            }
        }
        
        $data['categorias'] = $categorias;
        $data['jogo'] = $jogo;
        $this->load->view('templates/header', $data);
        $this->load->view('jogos/edit', $data);
        $this->load->view('templates/footer');
    }

    public function delete($id)
    {
        $data['jogos_item'] = $this->jogos_model->get_jogos($id);

        if (empty($data['jogos_item'])){
            $this->session->set_flashdata('delete', 'Este jogo não existe.');
            redirect('jogos/listar');
        }
        else{
            if($this->jogos_model->delete_jogos($id)){
                $this->session->set_flashdata('delete', 'Seu jogo foi removido com sucesso!');
                redirect('jogos/listar');
            }
        }
    }
}