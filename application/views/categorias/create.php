<?php echo validation_errors(); ?>

<?php if(isset($sucesso)){ ?>
    <br/><br/>
    <div class="alert alert-success"><?php echo $sucesso; ?></div>
    <br/><br/>
	<a href="<?php echo site_url('categorias/listar'); ?>">Voltar</a>
<?php } ?>

<div class="form-well">
    <?php echo form_open('categorias/create') ?>

            <div class="form-group">
                <label for="nome">Nome:</label>
                <input class="form-control" type="input" name="nome" />
            </div>

            <input class="btn btn-primary" type="submit" name="submit" value="Salvar categoria" />

    <?php echo form_close(); ?>
</div>