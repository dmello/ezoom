<div class="list-well">
    <?php if($this->session->flashdata('delete')) { ?>
    	<div class="alert alert-warning"><?php echo $this->session->flashdata('delete'); ?></div>
    <?php } ?>
    <?php foreach ($categorias as $categorias_item){ ?>
        <h3><?php echo $categorias_item['nome'] ?></h3>
        
        <div class="categoria_action_wrapper">
            <div class="categoria_detalha"><a href="<?php echo site_url('categorias/editar/'.$categorias_item['id']); ?>">Editar categoria</a></div>
            <div class="categoria_remove"><a href="<?php echo site_url('categorias/remover/'.$categorias_item['id']); ?>">Remover categoria</a></div>
        </div>
    <?php } ?>
    <button type="button" class="btn btn-primary" onclick="location.href='<?php echo site_url('categorias/criar'); ?>';">Inserir categoria</button>
</div>