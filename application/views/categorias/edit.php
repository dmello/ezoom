<?php echo validation_errors(); ?>

    <?php foreach ($categoria as $categoria_item) { ?>
        <div class="form-well">
            <?php echo form_open('categorias/edit/'.$categoria_item->id) ?>
                <input type="hidden" value="<?php echo $categoria_item->id; ?>" name="id">
                <div class="form-group">
                    <label for="nome">Nome:</label>
                    <input type="input" class="form-control" name="nome" value="<?php echo $categoria_item->nome; ?>"/>
                </div>

                <input type="submit" name="submit" class="btn btn-primary" value="Salvar alterações" />
            <?php echo form_close(); ?>
        </div>
    <?php } ?>


    <?php if(isset($sucesso)){ ?>
	    <br/><br/>
	    <?php echo $sucesso; ?>
	    <br/><br/>
    	<a href="<?php echo site_url('categorias/listar'); ?>">Voltar</a>
    <?php } ?>

</form>