<div class="list-well">
    <?php if($this->session->flashdata('delete')) { ?>
    	<div class="alert alert-warning"><?php echo $this->session->flashdata('delete'); ?></div>
    <?php } ?>
    <?php echo form_open('jogos/index'); ?>
        <div class="form-group">
            <div class="ordenar_wrapper">
                <select class="form-control ordenar-select" name="ordenar_jogos">
                    <option value="nome ASC">Nome Crescente</option>
                    <option value="nome DESC">Nome Decrescente</option>
                    <option value="ano_publicacao ASC">Data Crescente</option>
                    <option value="ano_publicacao DESC">Data Decrescente</option>
                </select>
                <button type="submit" class="btn btn-primary ordenar-botao">Ordenar</button>
            </div>
        </div>
    <?php echo form_close(); ?>
    </form>
    <?php foreach ($jogos as $jogos_item){ ?>
        <h3><?php echo $jogos_item['nome'] ?></h3>
        <div class="main">
            Ano publicação: <?php $date = new DateTime($jogos_item['ano_publicacao']); echo $date->format('d/m/Y'); ?><br/>
            <?php foreach ($categorias as $categoria_item) { ?>
                <?php if($jogos_item['id_categoria'] == $categoria_item['id']){ ?>
                    Categoria: <?php echo $categoria_item['nome']; ?>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="jogo_action_wrapper">
            <div class="jogo_detalha"><a href="<?php echo site_url('jogos/editar/'.$jogos_item['id']); ?>">Editar jogo</a></div>
            <div class="jogo_remove"><a href="<?php echo site_url('jogos/remover/'.$jogos_item['id']); ?>">Remover jogo</a></div>
        </div>
    <?php } ?>
    <button type="button" class="btn btn-primary" onclick="location.href='<?php echo site_url('jogos/criar'); ?>';">Inserir jogo</button>
</div>
