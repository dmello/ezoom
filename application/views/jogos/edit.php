<?php echo validation_errors(); ?>

    <?php foreach ($jogo as $jogo_item) { ?>
        <div class="form-well">
            <?php echo form_open('jogos/edit/'.$jogo_item->id) ?>
                <input type="hidden" value="<?php echo $jogo_item->id; ?>" name="id">
                <div class="form-group">
                    <label for="nome">Nome:</label>
                    <input type="input" class="form-control" name="nome" value="<?php echo $jogo_item->nome; ?>"/>
                </div>

                <div class="form-group">
                    <label for="categorias">Categorias:</label>
                    <select class="form-control" name="categorias">
                	    <?php foreach($categorias as $categoria_item){ ?>
                            <?php if($categoria_item['id'] == $jogo_item->id_categoria){ ?>
                    	    	<option value="<?php echo $categoria_item['id']; ?>" selected="selected"><?php echo $categoria_item['nome']; ?></option>
                            <?php }else{ ?>
                                <option value="<?php echo $categoria_item['id']; ?>"><?php echo $categoria_item['nome']; ?></option>
                            <?php } ?>
                	    <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="ano_publicacao">Ano de Publicação:</label>
                    <input type="input" class="form-control" name="ano_publicacao" value="<?php $date = new DateTime($jogo_item->ano_publicacao); 
                                                                       echo $date->format('d/m/Y'); ?>"/>
                </div>

                <div class="form-group">                                                       
                    <label for="descricao">Descrição:</label>
                    <textarea name="descricao" class="form-control"><?php echo $jogo_item->descricao; ?></textarea>
                </div>    

                <input type="submit" name="submit" class="btn btn-primary" value="Salvar alterações" />
            <?php echo form_close(); ?>
        </div>
    <?php } ?>


    <?php if(isset($sucesso)){ ?>
	    <br/><br/>
	    <?php echo $sucesso; ?>
	    <br/><br/>
    	<a href="<?php echo site_url('jogos/listar'); ?>">Voltar</a>
    <?php } ?>

</form>