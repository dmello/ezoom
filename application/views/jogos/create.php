<?php echo validation_errors(); ?>

<?php if(isset($sucesso)){ ?>
    <br/><br/>
    <div class="alert alert-success"><?php echo $sucesso; ?></div>
    <br/><br/>
	<a href="<?php echo site_url('jogos/listar'); ?>">Voltar</a>
<?php } ?>

<div class="form-well">
    <?php echo form_open('jogos/create') ?>

            <div class="form-group">
                <label for="nome">Nome:</label>
                <input class="form-control" type="input" name="nome" />
            </div>

            <div class="form-group">
                <label for="categorias">Categorias:</label>
                <select class="form-control" name="categorias">
                    <?php foreach($categorias as $categoria_item){ ?>
                        <option value="<?php echo $categoria_item['id'] ?>"><?php echo $categoria_item['nome']; ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label for="ano_publicacao">Ano de Publicação:</label>
                <input class="form-control" type="input" name="ano_publicacao" />
            </div>

            <div class="form-group">
                <label for="descricao">Descrição:</label>
                <textarea class="form-control" name="descricao"></textarea>
            </div>

            <input class="btn btn-primary" type="submit" name="submit" value="Salvar jogo" />

    <?php echo form_close(); ?>
</div>