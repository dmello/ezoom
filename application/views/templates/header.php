<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
		<meta charset="utf-8" />
		<?=link_tag('assets/styles.css');?>
		<?=link_tag('assets/css/bootstrap.min.css');?>
		<?=link_tag('assets/js/bootstrap.min.js');?>
        <title>Teste de CodeIgniter - Cadastro de Jogos</title>
    </head>
    <body>
    	<ul class="nav nav-pills" style="padding-left:20px;">
		    <li role="presentation" class="<?php echo ($this->uri->segment(1) == 'jogos') ? "active" : "" ?>"><a href="<?php echo site_url('jogos'); ?>">Jogos</a></li>
		    <li role="presentation" class="<?php echo ($this->uri->segment(1) == 'categorias') ? "active" : "" ?>"><a href="<?php echo site_url('categorias'); ?>">Categorias</a></li>
		</ul>
    	<div style="padding-left:20px;"><h1><?php echo $title ?></h1></div>